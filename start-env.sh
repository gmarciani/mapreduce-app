#!/bin/bash

##
# ENVARS
##
export APP_HOME="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

##
# HADOOP
##
${HADOOP_HOME}/sbin/stop-dfs.sh
${HADOOP_HOME}/sbin/stop-yarn.sh

if [ "$1" = "format" ]; then
    sudo rm -rf /tmp/hadoop*
    ${HADOOP_HOME}/bin/hdfs namenode -format -force
fi

${HADOOP_HOME}/sbin/start-dfs.sh

${HADOOP_HOME}/bin/hdfs dfs -mkdir /user/

${HADOOP_HOME}/bin/hdfs dfs -mkdir /user/app
${HADOOP_HOME}/bin/hdfs dfs -mkdir /user/app/input
${HADOOP_HOME}/bin/hdfs dfs -mkdir /user/app/output
${HADOOP_HOME}/bin/hdfs dfs -mkdir /user/app/test
${HADOOP_HOME}/bin/hdfs dfs -mkdir /user/app/test/input
${HADOOP_HOME}/bin/hdfs dfs -mkdir /user/app/test/output

${HADOOP_HOME}/bin/hdfs dfs -mkdir /tmp
${HADOOP_HOME}/bin/hdfs dfs -chmod g+w /tmp
