#!/bin/bash

##
# EVAL_Q1
##

APP_HOME="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"
APP_JAR="${APP_HOME}/target/app-1.0.jar"

HADOOP_JAR="${HADOOP_HOME}/bin/hadoop jar"
HDFS_WAREHOUSE="/user/app/input"
HDFS_OUT="/user/app/output/eval_q1"

HDFS_RM="${HADOOP_HOME}/bin/hdfs dfs -rm -r -f"

OUT_EVALUATION="eval_q1.out"
rm ${OUT_EVALUATION}

QUERIES=( "query1_1" )
SLEEP_BETWEEN_QUERIES=20s

for QUERY in "${QUERIES[@]}"; do
    OPT_TOPK_SIZE="-Dapp.topk.size"
    TOPK_SIZES=( "1" "2" "4" "8" "16" )
    IN_WORDS="${HDFS_WAREHOUSE}/words"

    for TOPK_SIZE in "${TOPK_SIZES[@]}"; do
        OUT_QUERY="${HDFS_OUT}/${QUERY}.${TOPK_SIZE}"
        $HDFS_RM "${OUT_QUERY}*"
        OPTS=""
        OPTS="${OPTS} ${OPT_TOPK_SIZE}=${TOPK_SIZE}"
        START="$( date +%s )"
        ${HADOOP_JAR} ${APP_JAR} ${QUERY} ${OPTS} ${IN_WORDS} ${OUT_QUERY}
        END="$( date +%s )"
        ELAPSED="$(( ${END} - ${START} ))"
        echo "${QUERY} (${OPTS}) : ${ELAPSED} seconds" >> ${OUT_EVALUATION}
        sleep ${SLEEP_BETWEEN_QUERIES}
    done
done
