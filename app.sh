#!/bin/bash

APP_HOME="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

APP_JAR="${APP_HOME}/target/app-1.0.jar"

##
# HADOOP COMMANDS
##
HADOOP_JAR="${HADOOP_HOME}/bin/hadoop jar"
HDFS_RM="${HADOOP_HOME}/bin/hdfs dfs -rm -r -f"

##
# INPUT
##
HDFS_WAREHOUSE="/user/app/input"
IN_WORDS="${HDFS_WAREHOUSE}/words"

##
# OUTPUT
##
HDFS_OUT="/user/app/output"

##
# QUERY 1.1
##
QUERY="query1_1"
OPT_TOPK_SIZE="-app.topk.size"
TOPK_SIZE=3
OPTS="${OPT_TOPK_SIZE}=${TOPK_SIZE}"
QUERY_OUT="${HDFS_OUT}/${QUERY}"
$HDFS_RM "${QUERY_OUT}*"
${HADOOP_JAR} ${APP_JAR} ${QUERY} ${OPTS} ${IN_WORDS} ${QUERY_OUT}

##
# QUERY 1.2
##
QUERY="query1_2"
OPT_TOPK_SIZE="-app.topk.size"
TOPK_SIZE=3
OPTS="${OPT_TOPK_SIZE}=${TOPK_SIZE}"
QUERY_OUT="${HDFS_OUT}/${QUERY}"
$HDFS_RM "${QUERY_OUT}*"
${HADOOP_JAR} ${APP_JAR} ${QUERY} ${OPTS} ${IN_WORDS} ${QUERY_OUT}
