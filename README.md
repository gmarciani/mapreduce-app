# HADOOP-SCAFFOLDING

*Scaffolding for Hadoop Map/Reduce applications*


## Requirements
The system needs to be provided with the following packages:
* Java >= 1.8.0
* Maven >= 3.3.9
* Hadoop = 2.8.0

and the following environment variables, pointing to the respective package home directory:
* JAVA_HOME
* MAVEN_HOME
* HADOOP_HOME


## Build
Build the map/reduce driver for all queries:

    $> mvn clean package


## Usage
Start the environment:

    $app_home> bash start-env.sh

If it is the first environment setup, you need to run:

    $app_home> bash start-env.sh format

WARNING: notice that the last command will format your HDFS and Hive metastore.

The general job submission is as follows:

    $hadoop_home> bin/hadoop jar <DRIVER_JAR> <PROGRAM> [HADOOP_OPTS] [PROGRAM_OPTS] <ARGS>

where
* **[DRIVER_JAR]** is the local absolute path to the driver JAR;
* **[PROGRAM]** is the name of the map/reduce program to execute;
* **[HADOOP_OPTS]** are optional Hadoop options (e.g. -Dopt=val);
* **[PROGRAM_OPTS]** are optional program options (e.g. -D opt=val);
* **[ARGS]** are the mandatory program arguments.

Notice that the following map/reduce programs are available:
* **query1_1** the 1st query, leveraging TEXT file format.
* **query1_2** the 1st query, leveraging ORC file format .

Read the output:

    $hadoop_home> bin/hadoop hdfs -cat [RESULT]/*

where
*[RESULT]* is the HDFS directory of results.

Stop the environment:

    $app_home> bash stop-env.sh


### Query1

    $app_home> bin/hadoop jar <DRIVER_JAR> query1 [HADOOP_OPTS] [PROGRAM_OPTS] <IN_RATINGS> <IN_MOVIES> <OUT>

where:
* **[DRIVER_JAR]** is the local absolute path to the driver JAR;
* **[HADOOP_OPTS]** are optional Hadoop options (e.g. -Dopt=val);
* **[PROGRAM_OPTS]** are optional program options (e.g. -D opt=val);
* **[IN_RATINGS]** is the HDFS absolute path to the directory containing the ratings data set;
* **[IN_MOVIES]** is the HDFS absolute path to the directory containing the movies data set;
* **[OUT]** is the HDFS absolute path to the directory for the output.

Important note: query1_1 accepts only Text input files, while query1_2 accepts only ORC input files.

The following program options are available:
* `app.topk.size`: the size of the top-k ranking;

Here is an example:

    $hadoop_home> bin/hadoop jar app-1.0.jar \
    query1_1 \
    -D app.topk.size=3 \
    /hdfs/path/to/words \
    /hdfs/path/to/output/query1_1


### Data Ingestion/Exportation

The environment setup activates both the data ingestion and exportation.
In particular it activates the following Flume agents:
* **movies_agent**: imports movies dataset from the local spooldir `/path/to/moviedoop/data/flume/movies` to the HDFS directory `/user/flume/movies` as an external Hive table in sequence file format;
* **movies_agent**: imports ratings dataset from the local spooldir `/path/to/moviedoop/data/flume/ratings` to the HDFS directory `/user/flume/ratings` as an external Hive table in  sequence file format;
* **query1_agent**: exports the results of query1 from `hdfs:///user/moviedoop/output/query1` to the HBase table `query1`;
* **query2_agent**: exports the results of query2 from `hdfs:///user/moviedoop/output/query2` to the HBase table `query2`;
* **query3_agent**: exports the results of query3 from `hdfs:///user/moviedoop/output/query3` to the HBase table `query3`.


## Evaluation
The performance of all queries can be evaluated running the bash scripts in folder `eval/`.
Every evaluation script compares the baseline and the optimized implementation of a specific query,
generating a report file in the same directory.
For user's convenience, all the evaluation reports have been included in `eval/out`
The following evaluation scripts are available:
* **eval_q1.sh**: evaluates query1.


## Authors
Giacomo Marciani, [gmarciani@acm.org](mailto:gmarciani@acm.org)


## References
Systems and Architectures for Big Data, course by prof. Valeria Cardellini. 2016/2017 [Read here](http://www.ce.uniroma2.it/courses/sabd1617/)


## License
The project is released under the [MIT License](https://opensource.org/licenses/MIT).
